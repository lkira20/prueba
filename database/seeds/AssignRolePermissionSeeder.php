<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AssignRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = Role::create(['name' => 'Super-admin']);
        //POST
        $permission = Permission::create(['name' => 'post.index']);
        $permission->assignRole($role);

        $permission = Permission::create(['name' => 'post.edit']);
        $permission->assignRole($role);

        $permission = Permission::create(['name' => 'post.show']);
        $permission->assignRole($role);

        $permission = Permission::create(['name' => 'post.delete']);
        $permission->assignRole($role);
        //COMMENT
        $permission = Permission::create(['name' => 'comment.index']);
        $permission->assignRole($role);

        $permission = Permission::create(['name' => 'comment.edit']);
        $permission->assignRole($role);

        $permission = Permission::create(['name' => 'comment.show']);
        $permission->assignRole($role);

        $permission = Permission::create(['name' => 'comment.delete']);
        $permission->assignRole($role);
    }
}
