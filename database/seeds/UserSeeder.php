<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new User();
        $user->name = "luis";
        $user->email = "luis@gmail.com";
        $user->password = bcrypt("12345678");
        $user->save();

        $user = new User();
        $user->name = "jose";
        $user->email = "jose@gmail.com";
        $user->password = bcrypt("12345678");
        $user->save();

        $user = new User();
        $user->name = "pedro";
        $user->email = "pedro@gmail.com";
        $user->password = bcrypt("12345678");
        $user->save();

        $user = new User();
        $user->name = "admin";
        $user->email = "admin@gmail.com";
        $user->password = bcrypt("12345678");
        $user->save();
        $user->assignRole('Super-admin');
    }
}
