<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;
use App\Post;
use App\User;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        //
        'post_id' => Post::all()->random()->id,
        'user_id' => User::all()->random()->id,
        'text' => $faker->text
    ];
});
