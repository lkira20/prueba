<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes

    Route::resource('/post', 'PostController');
    Route::get('/mis-post', 'PostController@mis_post');
    Route::resource('/comment', 'CommentController');

    Route::get('/perfil', 'PerfilController@index')->name('perfil.index');
    Route::get('/perfil/edit', 'PerfilController@edit')->name('perfil.edit');
    Route::put('/perfil/{id}', 'PerfilController@update')->name('perfil.update');
});
