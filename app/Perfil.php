<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    //
    protected $fillable = [
        'segundo_nombre', 'apellido', 'segundo_apellido', 'fecha_de_nacimiento', 'nacionalidad'
    ];
}
