<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Perfil;
use Illuminate\Support\Facades\Auth;

class PerfilController extends Controller
{
    //
    public function index()
    {
    	$id = Auth::id();

    	$perfil = Perfil::where('user_id', $id)->first();

    	if ($perfil == null) {
    		$perfil = new Perfil();
    		$perfil->user_id = $id;
    		$perfil->save();
    	}

    	$user = User::with('perfil')->findOrFail($id);

    	return view('perfil.index', compact('user'));
    }

    public function edit()
    {
    	$id = Auth::id();

    	$user = User::with('perfil')->findOrFail($id);

    	return view('perfil.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
    	$user = Auth::user();

        $user->update($request->all());

    	$perfil = Perfil::where('user_id', $id)->first()->update($request->all());

    	return back()->with('exito', 'perfil editado exitosamente');
    }
}
