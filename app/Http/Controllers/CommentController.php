<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $comment = new Comment();
        $comment->post_id = $request->post_id;
        $comment->user_id = Auth::id();
        $comment->text = $request->text;
        $comment->save();

        return redirect()->route('post.index')->with('comment', 'commentario creado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
        $user = Auth::user();

        if ($post->user_id == $user->id || $user->can('comment.edit')) {

            return view('comment.edit', compact('comment'));
        }else{

            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
        $user = Auth::user();

        if ($post->user_id == $user->id || $user->can('comment.edit')) {

            $comment->text = $request->text;
            $comment->save();

            return redirect()->route('post.index')->with('comment_edit', 'commentario editado exitosamente!');
        }else{

            return abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
        $user = Auth::user();

        if ($post->user_id == $user->id || $user->can('comment.delete')) {
            $comment->delete();

            return redirect()->route('post.index')->with('comment_delete', 'commentario eliminado exitosamente!');
        }else{

            return abort(403);
        }
    }
}
