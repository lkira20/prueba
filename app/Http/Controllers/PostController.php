<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::with('comments.user', 'user')->orderBy('created_at', 'desc')->paginate(5);

        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $post = new Post();
        $post->text = $request->text;
        $post->user_id = Auth::id();
        $post->save();

        return redirect()->route('post.index')->with('creado', 'post creado exitosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $user = Auth::user();

        if ($post->user_id == $user->id || $user->can('post.edit')) {
            # code...
        
            return view('post.edit', compact('post'));
        }else{

            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        $user = Auth::user();

        if ($post->user_id == $user->id || $user->can('post.edit')) {
            $post->text = $request->text;
            $post->save();

            return redirect()->route('post.index')->with('actualizado', 'actualizado exitosamente!');
        }else{

            return abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        $user = Auth::user();
        
        if ($post->user_id == $user->id || $user->can('post.delete')) {
            $post->delete();

            return back()->with('eliminado', 'eliminado exitosamente!');
        }else{

            return abort(403);
        }
    }

    public function mis_post()
    {
        $id = Auth::id();

        $posts = Post::with('comments.user', 'user')->where('user_id', $id)->orderBy('created_at', 'desc')->paginate(5);

        return view('post.mis_post', compact('posts'));
    }
}
