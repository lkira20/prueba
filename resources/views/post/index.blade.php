@extends('adminlte::layouts.app')

@section('main-content')
	<div class="container-fluid spark-screen">
		@if (session('creado'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('creado') }}
		    </div>
		@endif

		@if (session('actualizado'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('actualizado') }}
		    </div>
		@endif

		@if (session('eliminado'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('eliminado') }}
		    </div>
		@endif

		@if (session('comment'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('comment') }}
		    </div>
		@endif

		@if (session('comment_edit'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('comment_edit') }}
		    </div>
		@endif

		@if (session('comment_delete'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('comment_delete') }}
		    </div>
		@endif

		<div class="row">
			<div class="col-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">¿Que estas pensando?</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form action="/post" method="post">
							@csrf
							<textarea name="text" cols="30" rows="10" class="form-control" maxlength="256"></textarea>
							<button class="btn btn-primary pull-right mt-2" type="submit">Postear</button>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>

			<div class="col-12">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Feed</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@foreach($posts as $post)
						<!--POST-->
						<ul class="media-list">
						  	<li class="media">
						    	<div class="media-left">
						      		<a href="#">
						        		<i class="fa fa-user" style="font-size: 5em;"></i>
						      		</a>
						    	</div>
						    	<div class="media-body">
						    		<div class="row">
						    			<div class="col-md-9">
						    				<h4 class="media-heading">{{$post->user->name}}</h4>
						    			</div>
						    			<div class="col-md-3">
						    				@if($post->user_id == Auth::id() || (Auth::user()->can('post.delete') && Auth::id()|| Auth::user()->can('post.edit')))
						    					<!--Editar post-->
						    					<div class="row row-no-gutters">
						    						<div class="col-md-6" style="padding-left: 0px;">
								    					
								    					<a href="/post/{{$post->id}}/edit" class="btn btn-warning pull-right">Editar</a>
							    					</div>
							    					<!--Eliminar post-->
							    					<div class="col-md-6" style="padding-left: 0px;">
								    					<form action="/post/{{$post->id}}" method="post" >
								    						<input type="hidden" name="_method" value="delete">
														    <input type="hidden" name="_token" value="{{ csrf_token() }}">

								    						<button class="btn btn-danger float-right text-right" class="pull-right">eliminar</button>
								    					</form>
							    					</div>
						    					</div>
						    				@endif
						    			</div>
						    		</div>
						    		
						      		
						      		{{$post->text}}
						      		<!--COMMENT-->
						      		<ul class="media-list">
						      			@foreach($post->comments as $comment)
									  	<li class="media">
									    	<div class="media-left">
									      		<a href="#">
									        		<i class="fa fa-user" style="font-size: 5em;"></i>
									      		</a>
									    	</div>
									    	<div class="media-body">
									    		<div class="row">
									    			<div class="col-md-9">
									    				<h4 class="media-heading">{{$comment->user->name}}</h4>
									    			</div>
									    			<div class="col-md-3">
									    				@if($comment->user_id == Auth::id()|| (Auth::user()->can('post.delete') && Auth::id()|| Auth::user()->can('post.edit')))
									    					<!--Editar post-->
									    					<div class="row row-no-gutters">
									    						<div class="col-md-6" style="padding-left: 0px;">
											    					
											    					<a href="/comment/{{$comment->id}}/edit" class="btn btn-warning pull-right">Editar</a>
										    					</div>
										    					<!--Eliminar post-->
										    					<div class="col-md-6" style="padding-left: 0px;">
											    					<form action="/comment/{{$comment->id}}" method="post" >
											    						<input type="hidden" name="_method" value="delete">
																	    <input type="hidden" name="_token" value="{{ csrf_token() }}">

											    						<button class="btn btn-danger float-right text-right" class="pull-right">eliminar</button>
											    					</form>
										    					</div>
									    					</div>
									    				@endif
									    			</div>
									    		</div>

									      		
									      		{{$comment->text}}
									      	
									    	</div>
									  	</li>
									  	@endforeach
									  	<li class="media">
									    	<div class="media-left">
									      		<a href="#">
									        		<i class="fa fa-user" style="font-size: 5em;"></i>
									      		</a>
									    	</div>
									    	<div class="media-body">
									      		<form action="/comment" method="post">
									      			<div class="row">
													  	<div class="col-md-10">

											   				@csrf
													      	<input type="text" class="form-control" aria-label="..." placeholder="Escriba su comentario" name="text">
													      	<input type="hidden" name="post_id" value="{{$post->id}}">
													    	
													  	</div><!-- /.col-lg-6 -->
													  	<div class="col-md-2">
						
													      	<button class="btn btn-primary" type="submit">Comentar</button>
													    	
													  	</div><!-- /.col-lg-6 -->
													</div><!-- /.row -->
									      		</form>
									    	</div>
									  	</li>
									</ul>
						    	</div>
						  	</li>
						</ul>
						<hr>
						@endforeach

						<div>
							{{$posts->render()}}
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

@endsection