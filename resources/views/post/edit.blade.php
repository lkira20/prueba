@extends('adminlte::layouts.app')

@section('main-content')
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">¿Que estas pensando?</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
					<i class="fa fa-minus"></i></button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
					<i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">
			<form action="/post/{{$post->id}}" method="post">
				@method('PUT')
				@csrf
				<textarea name="text" cols="30" rows="10" class="form-control" maxlength="256">{{$post->text}}</textarea>
				<button class="btn btn-primary pull-right mt-2" type="submit">Editar</button>
			</form>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
@endsection