@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">

		@if (session('exito'))
		    <div class="alert alert-success alert-dismissible">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        {{ session('exito') }}
		    </div>
		@endif

		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Editar perfil</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form method="post" action="/perfil/{{$user->id}}">
							@method('PUT')
							@csrf
						  	<div class="form-group">
						    	<label for="exampleInputEmail1">Correo</label>
					    		<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" value="{{$user->email}}">
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInput1">Nombre</label>
					    		<input type="text" class="form-control" id="exampleInput1" placeholder="Nombre" name="name" value="{{$user->name}}">
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInput2">Segundo nombre</label>
					    		<input type="tex" class="form-control" id="exampleInput2" placeholder="segundo nombre" name="segundo_nombre" value="{{$user->perfil->segundo_nombre}}">
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInput3">Apellido</label>
					    		<input type="text" class="form-control" id="exampleInput3" placeholder="Apellido" name="apellido" value="{{$user->perfil->apellido}}">
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInput4">Segundo apellido</label>
					    		<input type="tex" class="form-control" id="exampleInput4" placeholder="Segundo apellido" name="segundo_apellido" value="{{$user->perfil->segundo_apellido}}">
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInput5">Fecha de nacimiento</label>
					    		<input type="date" class="form-control" id="exampleInput5" placeholder="Fecha de nacimiento" name="fecha_de_nacimiento" value="{{$user->perfil->fecha_de_nacimiento}}">
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInput6">Pais de nacimiento</label>
					    		<input type="text" class="form-control" id="exampleInput6" placeholder="Nacionalidad" name="nacionalidad" value="{{$user->perfil->nacionalidad}}">
						  	</div>
						  	
						  	
						  	<button type="submit" class="btn btn-primary">Editar</button>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
