@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Perfil</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="media">
							<div class="media-left media-middle">
						    	<a href="#">
						      		<i class="fa fa-user" style="font-size: 15em;"></i>
						    	</a>
						  	</div>
						  	<div class="media-body">
						  		<div class="pull-right">
						  		<a class="btn btn-warning" href="/perfil/edit">Editar</a>
						  		</div>
						    	<h4 class="media-heading">Datos:</h4>
						    	<p><strong>Nombre:</strong> {{$user->name}}</p>
						    	<p><strong>Correo:</strong> {{$user->email}}</p>
						    	<p><strong>Segundo nombre:</strong> {{$user->perfil->segundo_nombre}}</p>
						    	<p><strong>Apellido:</strong> {{$user->perfil->apellido}}</p>
						    	<p><strong>Segundo apellido:</strong> {{$user->perfil->segundo_apellido}}</p>
						    	<p><strong>Fecha de nacimiento:</strong> {{$user->perfil->fecha_de_nacimiento}}</p>
						    	<p><strong>Nacionalidad:</strong> {{$user->perfil->nacionalidad}}</p>
						  	</div>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
